import { Component, OnInit } from '@angular/core';
import { Hero, Goals, Newsletter } from './models/Ihome';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public hero: Hero;
  public goals: Goals;
  public newsletter: Newsletter;
  constructor() {
    this.hero = {
      logo: {
        url: './assets/img/pantone2.png',
        alt: 'logo Pantone'
      },
      titleOne: 'Aquí empieza todo',
      titleTwo: ' ¿Necesitas una Web?',
      img: {
        url: './assets/img/design.png',
        alt: 'diseño pantone',
      },
      description:'Cada negocio es diferente, por eso cada web también debe serlo. Paga sólo por lo que tu negocio necesita, calcula el presupuesto de tu página web en función de las necesidades de tu empresa.',
    }
    this.goals = {
      prices: [
        {
          model: 'Web Básica',
          description: 'Única página deslizante con 3 secciones incluidas, 3 cuentas de correo electrónico incluidas, asistencia técnica vía email.',
          price: 350,
        },
        {
          model: 'Web Avanzada',
          description: 'Menú principal con 6 secciones,5 cuentas de correo electrónico incluidas, diseño web personalizado,asistencia técnica vía email.',
          price: 690,
        },
        {
          model: 'Web Premium',
          description: 'Menú principal con 8 secciones,8 cuentas de correo electrónico incluidas, diseño web personalizado,asistencia técnica vía telefónica.',
          price: 999,
        },
      ],
      features: [
        {
          title: 'Diseño de Páginas web profesionales',
          description: 'El éxito no está en lo que se hace, si no en como se hace. Nos involucramos para conseguir el éxito de nuestros clientes. Nos apasiona lo que hacemos y es por ello que realizamos páginas web de gran calidad, sabemos lo que hacemos, y lo sabemos hacer muy bien.' ,
          img: {
            url:'./assets/img/web.png',
            alt:'dieño web pantone'
          }
        },
        {
          title: 'Posicionamiento SEO',
          description: 'Optimizamos tu página web para que obtengas las primeras posiciones en Google. Aplicaremos las mejoras necesarias y óptimas para incrementar un mejor rendimiento de tu web, rapidez de carga, medición de palabras clave…' ,
          img: {
            url:'./assets/img/seo.jpg',
            alt:'seo pantone'
          }
        },
        {
          title: 'Redes Sociales',
          description: 'Llevamos a cabo tus campañas de publicidad en las redes sociales. Evaluamos la demanda y aplicamos conocimientos para aumentar el tráfico en tu web. Hacemos Community manager para pequeñas y grandes empresas.' ,
          img: {
            url:'./assets/img/rrss.jpg',
            alt:'rrss pantone'
        },
        },
      ],
   }
   
}
ngOnInit(): void {

}
public subscribeNewsletter(newsletter: Newsletter) {
  this.newsletter = newsletter;
}
}