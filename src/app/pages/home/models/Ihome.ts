export interface Hero {
    logo: Img;
    titleOne: string;
    titleTwo:string;
    img: Img;
    description: string;
}

export interface Img {
    url: string;
    alt: string;
}

export interface Goals {
    prices: Prices[];
    features: Features[];
}

export interface Prices {
    model: string;
    description: string;
    price: number;
}

export interface Features {
    title: string;
    description: string;
    img: Img;
}

export interface Newsletter{
    name: string;
    mail: string;
    phone: number;
    web: 'básica'|'avanzada'|'premium';
}