import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Newsletter } from './../models/Ihome';
import { Component, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-newsletter',
  templateUrl: './newsletter.component.html',
  styleUrls: ['./newsletter.component.scss']
})
export class NewsletterComponent implements OnInit {
  public newsletterForm: FormGroup = null;
  public submitted: boolean = false;
  @Output() public newsletterEmit = new EventEmitter <Newsletter>();
  constructor(private formBuilder: FormBuilder) {
    this.newsletterForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(1)]],
      mail: ['', [Validators.required, Validators.email]],
      phone: ['',[Validators.required,Validators.min(600000000)]],
      web: [''],
    });
   }

  ngOnInit(): void {
  }
  public onSubmit(): void {
    this.submitted = true;
    if (this.newsletterForm.valid) {
      const newsletterRegister: Newsletter = {
        name: this.newsletterForm.get('name').value,
        mail: this.newsletterForm.get('mail').value,
        phone: this.newsletterForm.get('phone').value,
        web: this.newsletterForm.get('web').value,
      };
      this.sendNewsletter(newsletterRegister);
      console.log(newsletterRegister);
      this.newsletterForm.reset();
      this.submitted = false;
    }
  }

  public sendNewsletter(newsletterRegister: Newsletter) {
      this.newsletterEmit.emit(newsletterRegister);
  }
}